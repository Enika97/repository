from selenium import webdriver
from bs4 import BeautifulSoup
import urllib.request
import time
# import httlip
# from BeautifulSoup import BeautifulSoup
import pandas as pd


driver = webdriver.Chrome("E:\eni\chromedriver.exe")
products=[] #List to store name of the product
prices=[] #List to store price of the product
ratings=[] #List to store rating of the product
# url = data[1].find('a').get('href')
# url='https://www.guggenheim.org/artwork/artwork_type/filmvideo'
pretext_link='https://www.guggenheim.org'
# page = urllib.request.urlopen(url)
# driver.get('https://www.guggenheim.org/artwork/artwork_type/filmvideo')
# page = driver.page_source
# soup = BeautifulSoup(page,features="lxml")
# # # print(soup.prettify())
# tags=soup.find_all('article',class_="kzh_k64_k66 span-four")
# # print(tags)
# tags=soup.find_all('article',class_="kzh_k64_k66 span-four")
data={'Artist':[],'Title':[],'Date':[],'Medium':[],'Dimensions':[],'Credit line':[],'Accession':[],'Copy Rights':[],'Artwork Type':[]}

dataset=pd.DataFrame(data)
special_link=['https://www.guggenheim.org/artwork/special_collection/deutsche-guggenheim-commissions']

# links=[
# 'https://www.guggenheim.org/artwork/special_collection/karl-nierendorf-estate',
# 'https://www.guggenheim.org/artwork/special_collection/solomon-r-guggenheim-founding-collection',
# 'https://www.guggenheim.org/artwork/special_collection/the-panza-collection',
# 'https://www.guggenheim.org/artwork/special_collection/deutsche-guggenheim-commissions',
# 'https://www.guggenheim.org/artwork/special_collection/katherine-s-dreier-bequest']

links=['https://www.guggenheim.org/artwork/special_collection/collections-council',
'https://www.guggenheim.org/artwork/special_collection/karl-nierendorf-estate',
'https://www.guggenheim.org/artwork/special_collection/solomon-r-guggenheim-founding-collection',
'https://www.guggenheim.org/artwork/special_collection/the-panza-collection',
'https://www.guggenheim.org/artwork/special_collection/deutsche-guggenheim-commissions',
'https://www.guggenheim.org/artwork/special_collection/katherine-s-dreier-bequest',
'https://www.guggenheim.org/artwork/special_collection/thannhauser-collection',
'https://www.guggenheim.org/artwork/special_collection/the-robert-h-n-ho-family-foundation-collection',
'https://www.guggenheim.org/artwork/special_collection/guggenheim-ubs-map-purchase-fund',
'https://www.guggenheim.org/artwork/special_collection/peggy-guggenheim-collection',
'https://www.guggenheim.org/artwork/special_collection/the-bohen-foundation-gift',
'https://www.guggenheim.org/artwork/special_collection/the-robert-mapplethorpe-foundation-gift',
'https://www.guggenheim.org/artwork/special_collection/international-directors-council',
'https://www.guggenheim.org/artwork/special_collection/photography-council',
'https://www.guggenheim.org/artwork/special_collection/the-hilla-rebay-collection',
'https://www.guggenheim.org/artwork/special_collection/young-collectors-council']

def re_group(s):
    a=''
    for member in s:
        a= a +  member+' '
    return a
def get_data(a):

    b=a.split('Artist')
    a = re_group(b)
    b=a.split('Title')
    artist=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Date')
    title=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Medium')
    date=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Dimensions')
    medium=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Credit Line')
    dimensions=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Accession')
    credit_line=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Copyright')
    accession=b[0]
    b.pop(0)
    
    a = re_group(b)
    b=a.split('Artwork Type')
    copy_rights=b[0]
    b.pop(0)
    if b!=[]:
        artwork_type=b[0]
        b.pop(0)
    else:
        artwork_type=''
    return artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type
def collect_links_data(links,pretext_link,dataset):
    driver = webdriver.Chrome("E:\eni\chromedriver.exe")
    for u_index,url in enumerate(links):
        print('/n/n <<<<<<<<<<we are at link ',u_index+1,'/',len(links),'>>>>>>>>>>/n/n')
        # page = urllib.request.urlopen(url)
        # driver.manage().addCookie(url)
        
        driver.get(url)
        time.sleep(2)
        # driver.add_cookie()
        # webdriver.manaaddCookie
        # driver.add_cookie(url)
        page = driver.page_source
        # article class="c89_eh1_eh3 container"
        soup = BeautifulSoup(page,features="lxml")
        tags=soup.find_all('article',class_="a6j_bfa_bfc container")
        while tags == None:
            driver.get(url)
            time.sleep(3)
            # driver.add_cookie()
            # webdriver.manaaddCookie
            # driver.add_cookie(url)
            page = driver.page_source
            # article class="c89_eh1_eh3 container"
            soup = BeautifulSoup(page,features="lxml")
            tags=soup.find_all('article',class_="a6j_bfa_bfc container")
        # inner_driver = webdriver.Chrome("/Users/aalmohamad/Desktop/sound french/chromedriver")
        # for i in tags :
        #     i_f=i.find_all(class_="space-50-above")
        #     for s in i_f:
        #         url=s.find_all('a')
        #         for art in url :
        #             a_l=art.get('href')
        #             print("tags found are",a_l)
        #             link=pretext_link+a_l
        #             inner_driver.get(link)
        #             artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
        #             print("data found are: ",artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type)
        #     break
        # a_l = data[1].find('a').get('href')
        
        inner_driver = webdriver.Chrome("E:\eni\chromedriver.exe")
        print("\n\nlength of tags is : ",len(tags))
        # print(tags)
        for t in tags:
            # jjm_jq9_jrb span-four
            c=t.find_all('nav',class_="emb_etz_et1")
            print('length of c is: ',len(c))
            for c_f in c:
                c_p=c_f.find_all('ul')
                # print('length of c_f is: ',len(c_f))
                for u in c_p:
                    pages_count=u.find_all('li')
                    print("\n\n\n pages count in ", url,' is : ',len(pages_count)-1)
            if len(c)>0:
                numer_of_recorded=0
                link_page=url+'/page/'
                for page_num in range(1,len(pages_count)):
                    print("\n\n\n  now moving in  ", url,' to page  : ',page_num,"/",len(pages_count)-1)
                    inner_link_page=link_page+str(page_num)
                    driver.get(inner_link_page)
                    time.sleep(2)
                    page = driver.page_source
                    soup = BeautifulSoup(page,features="lxml")
                    tags=soup.find_all('article',class_="a6j_bfa_bfc container")

                    while tags==None:
                        driver.get(link_page)
                        time.sleep(3)
                        page = driver.page_source
                        soup = BeautifulSoup(page,features="lxml")
                        tags=soup.find_all('article',class_="a6j_bfa_bfc container")
                    # /page/2
                    for inner_t in tags:
                        i_f=inner_t.find_all(class_="space-50-above")
                        for s in i_f:
                            url_a=s.find_all('a')
                            for art in url_a :
                                a_l=art.get('href')
                                print("tags found are",a_l)
                                link=pretext_link+a_l

                        # link=pretext_link+t.a['href']
                        # print(pretext_link+t.a['href'])
                        
                                inner_driver.get(link)
                                time.sleep(2)

                        # inner_driver.addCookie(,)
                                inner_page = inner_driver.page_source
                                inner_soup = BeautifulSoup(inner_page,features="lxml")
                                inner_tags=inner_soup.find('aside',class_="axw_a5k_a5m span-four space-15-above")
                                while inner_tags==None:
                                    print('In while loop dude !!!!!!!!!!!!!!!!!!!!!!!!!')
                                    inner_driver.get(link)
                                    time.sleep(3)
                                    # inner_driver.addCookie(,)
                                    inner_page = inner_driver.page_source
                                    inner_soup = BeautifulSoup(inner_page,features="lxml")
                                    inner_tags=inner_soup.find('aside',class_="axw_a5k_a5m span-four space-15-above")
                                tag_dl=inner_tags.dl.text
                                # print(">>>>>>>>>>",tag_dl)
                                artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
                                numer_of_recorded+=1
                                new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
                                print(">>>>>>>>>>>>>>>>>data found are: ",numer_of_recorded)
                                dataset = dataset.append(new_row, ignore_index=True)
                                # inner_driver.close()
            
                                # print(dataset)
            else:
                numer_of_recorded=0
                print("\n\n\n <<<<<<<<<<<<<only one page at : ",url)
                for inner_t in tags:
                    i_f=inner_t.find_all(class_="space-50-above")
                    for s in i_f:
                        url_a=s.find_all('a')
                        for art in url_a :
                            a_l=art.get('href')
                            print("tags found are",a_l)
                            link=pretext_link+a_l

                    # link=pretext_link+t.a['href']
                    # print(pretext_link+t.a['href'])
                    
                            inner_driver.get(link)
                            time.sleep(2)

                    # inner_driver.addCookie(,)
                            inner_page = inner_driver.page_source
                            inner_soup = BeautifulSoup(inner_page,features="lxml")
                            inner_tags=inner_soup.find('aside',class_="axw_a5k_a5m span-four space-15-above")
                            while inner_tags==None:
                                print('In while loop dude !!!!!!!!!!!!!!!!!!!!!!!!!')
                                inner_driver.get(link)
                                time.sleep(3)
                                # inner_driver.addCookie(,)
                                inner_page = inner_driver.page_source
                                inner_soup = BeautifulSoup(inner_page,features="lxml")
                                inner_tags=inner_soup.find('aside',class_="axw_a5k_a5m span-four space-15-above")
                            tag_dl=inner_tags.dl.text
                            # print(">>>>>>>>>>",tag_dl)
                            artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
                            numer_of_recorded+=1
                            new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
                            print(">>>>>>>>>>>>>>>>>data found are: ",numer_of_recorded)
                            dataset = dataset.append(new_row, ignore_index=True)

        dataset.to_excel(r"E:\eni project\5july\collectedData_5July.xlsx", engine='xlsxwriter')
        # driver.close()
    return dataset
            # new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
            # dataset = dataset.append(new_row, ignore_index=True)
            # inner_driver.close()
            # break
def collect_links_data_special(links,pretext_link,dataset):
    driver = webdriver.Chrome("/Users/aalmohamad/Desktop/sound french/chromedriver")
    for url in links:
        # page = urllib.request.urlopen(url)
        # driver.manage().addCookie(url)
        
        driver.get(url)
        time.sleep(3)
        # driver.add_cookie()
        # webdriver.manaaddCookie
        # driver.add_cookie(url)
        page = driver.page_source
        soup = BeautifulSoup(page,features="lxml")
        tags=soup.find_all('article',class_="kzh_k64_k66 span-four")
        inner_driver = webdriver.Chrome("/Users/aalmohamad/Desktop/sound french/chromedriver")
        # print(tags)
        for t in tags:
            
            
            link=pretext_link+t.a['href']
            print(pretext_link+t.a['href'])
            
            inner_driver.get(link)
            time.sleep(1)
            # inner_driver.addCookie(,)
            inner_page = inner_driver.page_source
            inner_soup = BeautifulSoup(inner_page,features="lxml")
            inner_tags=inner_soup.find('aside',class_="bu6_b2u_b2w span-four space-15-above")
            while inner_tags==None:
                print('In while loop dude !!!!!!!!!!!!!!!!!!!!!!!!!')
                inner_driver.get(link)
                time.sleep(3)
                # inner_driver.addCookie(,)
                inner_page = inner_driver.page_source
                inner_soup = BeautifulSoup(inner_page,features="lxml")
                inner_tags=inner_soup.find('aside',class_="bu6_b2u_b2w span-four space-15-above")
            tag_dl=inner_tags.dl.text
            # print(">>>>>>>>>>",tag_dl)
            artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
            new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
             
            dataset = dataset.append(new_row, ignore_index=True)
            # inner_driver.close()
            # print(dataset)
        
    return dataset
# for t in tags:
#     link=pretext_link+t.a['href']
#     print(pretext_link+t.a['href'])
#     inner_driver = webdriver.Chrome("/Users/aalmohamad/Desktop/sound french/chromedriver")
#     inner_driver.get(link)
#     inner_page = inner_driver.page_source
#     inner_soup = BeautifulSoup(inner_page,features="lxml")
#     inner_tags=inner_soup.find('aside',class_="bu6_b2u_b2w span-four space-15-above")
#     tag_dl=inner_tags.dl.text
#     # print(">>>>>>>>>>",tag_dl)
#     artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
#     new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
#     dataset = dataset.append(new_row, ignore_index=True)
#     print(new_row)
#     # inner_driver.close()
    
def collect_data(dataset,home_link,pretext_link):
    pretext_link='https://www.guggenheim.org'
    # page = urllib.request.urlopen(url)
    
    
    driver.get(home_link)
    page = driver.page_source
    soup = BeautifulSoup(page,features="lxml")


    nav=soup.find('nav',class_="nav-bar space-25-above")
    tag_ul=nav.ul.find('li',class_="nav-subtypes")
    # print(soup.prettify())
    # tags=soup.find_all('article',class_="kzh_k64_k66 span-four")
    # print(tags)

    # tags=soup.find_all('ul',class_="nav-items")
    # nav class="nav-bar space-25-above"
    # .ul.find
    # li class="nav-subtypes"

    # outer_tags=soup.find_all('li',class_="active nav-subtypes")

    tags=tag_ul.find_all('ul',role="navigation",class_="nav-items")
    # role="navigation"
    # driver.close()
    for m in tags:
        partial_link=m.li.a['href']
        link=pretext_link+partial_link
        print('>>>>>>>>',link)
        driver.get(link)
        page = driver.page_source
        soup = BeautifulSoup(page,features="lxml")
        in_tags=soup.find_all('article',class_="kzh_k64_k66 span-four")
        # driver.close()
        for t in in_tags:
            link=pretext_link+t.a['href']
            print(link)
            inner_driver = webdriver.Chrome("/Users/aalmohamad/Desktop/sound french/chromedriver")
            inner_driver.get(link)
            inner_page = inner_driver.page_source
            inner_soup = BeautifulSoup(inner_page,features="lxml")
            inner_tags=inner_soup.find('aside',class_="bu6_b2u_b2w span-four space-15-above")
            tag_dl=inner_tags.dl.text
            # print(">>>>>>>>>>",tag_dl)
            artist,title,date,medium,dimensions,credit_line,accession,copy_rights,artwork_type=get_data(tag_dl)
            new_row={'Artist':artist,'Title':title,'Date':date,'Medium':medium,'Dimensions':dimensions,'Credit line':credit_line,'Accession':accession,'Copy Rights':copy_rights,'Artwork Type':artwork_type}
            dataset = dataset.append(new_row, ignore_index=True)
            print(dataset)
            # inner_driver.close()
    
    return dataset
# home_link='https://www.guggenheim.org/artwork/artist/andy-warhol'
final_data_set=collect_links_data(links,pretext_link,dataset)
# final_data_set.to_excel("/Users/aalmohamad/Documents/Eni_Project/26May/collectedData_26May.xlsx", engine='xlsxwriter')


# for a in soup.findAll('a',href=True):
    # print(a)
    # name=a.find('div', attrs={'class':'_3wU53n'})
    # price=a.find('div', attrs={'class':'_1vC4OE _2rQ-NK'})
    # rating=a.find('div', attrs={'class':'hGSR34 _2beYZw'})
    # products.append(name.text)
    # prices.append(price.text)
    # ratings.append(rating.text) 
# df = pd.DataFrame({'Product Name':products,'Price':prices,'Rating':ratings}) 
# df.to_csv('products.csv', index=False, encoding='utf-8')
